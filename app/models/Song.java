package models;

import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Esta clase define el modelo Song en la base de datos con la informacion correspondiente para reproducir canciones
 *
 * Created by Makabeo on 22/05/2017.
 */
@Entity
public class Song
{
    @Id
    @GeneratedValue
    private int id;

    private int user_id;

    private String name;

    private String path;

    private String genre;

    public Song()
    {
        this.user_id = 0;
        this.name = "";
        this.path = "";
        this.genre = "";
    }

    public Song(int user_id, String name, String path, String genre)
    {
        this.user_id = user_id;
        this.name = name;
        this.path = path;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    /**
     * Método que guarda una cancion en la base de datos
     *
     */
    public void save()
    {
        JPA.em().persist(this);
    }

    /**
     * Método que devuelve una cancion dada su id
     * @return una cancion conservada en la base datos
     */
    public Song find(int id)
    {
        return JPA.em().find(Song.class,id);
    }
}
