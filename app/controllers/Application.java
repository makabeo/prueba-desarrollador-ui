package controllers;

import play.*;
import play.mvc.*;

import play.twirl.api.Html;
import views.html.*;
import views.html.homeSite.footer;
import views.html.homeSite.header;
import views.html.homeSite.home;


public class Application extends Controller {

    public static Result index() {
        Html headerSite = header.render();
        Html footerSite = footer.render();
        Html contenido = home.render();
        return ok(index.render(headerSite,footerSite,contenido));
    }
}