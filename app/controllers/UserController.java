package controllers;

import models.Song;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;
import views.html.*;
import views.html.auth.login;
import views.html.auth.register;
import views.html.homeSite.footer;
import views.html.homeSite.header;

import java.io.File;
import java.util.List;


/**
 * Created by Makabeo on 21/05/2017.
 */
public class UserController extends Controller
{

    public static Result register() {
        if (session("user") != null)
        {
            flash("success", "You have been successfully Logged M. " + session("user"));
            return redirect("/dashboard");
        }
        else
        {
            Html headerSite = header.render();
            Html footerSite = footer.render();
            Html contenido = register.render();
            return ok(index.render(headerSite,footerSite,contenido));
        }
    }

    public static Result login() {
        if (session("user") != null)
        {
            flash("success", "You have been successfully Logged Mr " + session("user"));
            return redirect("/dashboard");
        }
        else
        {
            Html headerSite = header.render();
            Html footerSite = footer.render();
            Html contenido = login.render();
            return ok(index.render(headerSite,footerSite,contenido));
        }
    }

    public static Result logout() {
        session().remove("user");
        flash("success", "You have been successfully logged out");
        return redirect("/");
    }

    @Transactional
    public static Result createUser()
    {
        DynamicForm form = Form.form().bindFromRequest();

        if (!(form.data().size() == 0))
        {
            String name = form.get("name");
            String username = form.get("username");
            String email = form.get("email");
            String password = form.get("password");
            String confirmPassword = form.get("confirmPassword");
            if (password.equals(confirmPassword))
            {
                User usuarioNuevo = new User(name,username, password,email);
                usuarioNuevo.save();
                flash("success", "You have been successfully registered");
                return redirect("/register");
            }
            else
            {
                flash("error", "The password are not equals");
                return redirect("/register");
            }

        }
        else
        {
            flash("error", "Please fill in the fields");
            return redirect("/register");
        }
    }

    @Transactional(readOnly=true)
    public static Result validate()
    {

        DynamicForm form = Form.form().bindFromRequest();


        if (!(form.data().size() == 0))
        {
            String username = form.get("username");
            String password = form.get("password");
            List<User> usuarios = JPA.em().createQuery(
                    "SELECT DISTINCT u FROM User u WHERE username LIKE :custUsername AND password LIKE :custPass")
                    .setParameter("custUsername", username)
                    .setParameter("custPass", password)
                    .getResultList();
            if (usuarios != null || usuarios.size() < 0)
            {
                try
                {
                    session("user", usuarios.get(0).getUsername());
                    flash("success", "You have been successfully Logged Mr. " + session("user"));
                    return redirect("/dashboard");
                }
                catch (Exception e)
                {
                    flash("error", "Combination of username and password is wrong");
                    return redirect("/login");
                }
            }
            else
            {
                flash("error", "Combination of username and password is wrong");
                return redirect("/login");
            }
        }
        else
        {
            flash("error", "Please fill in the fields");
            return redirect("/login");
        }
    }

    @Transactional
    public static Result deleteUser()
    {
        DynamicForm form = Form.form().bindFromRequest();
        String usuarioid = form.get("usuarioid");
        if (!usuarioid.equals(null) || !usuarioid.equals(""))
        {
            List<Song> canciones = JPA.em().createQuery(
                    "SELECT s FROM Song s WHERE user_id = :custUserId")
                    .setParameter("custUserId", Integer.parseInt(usuarioid))
                    .getResultList();

            for (int i = 0; i < canciones.size(); i++)
            {
                File file = new File("public/"+canciones.get(i).getPath());
                file.delete();
            }

            JPA.em().createQuery(
                    "DELETE FROM Song  WHERE user_id = :custUserId")
                    .setParameter("custUserId", Integer.parseInt(usuarioid))
                    .executeUpdate();
            JPA.em().createQuery(
                "DELETE FROM User  WHERE id = :custUserId")
                .setParameter("custUserId", Integer.parseInt(usuarioid))
                .executeUpdate();
            session().remove("user");
            flash("success", "You have been successfully deleted we miss you");
            return redirect("/");
        }
        else
        {
            flash("error", "user not found");
            return redirect("/dashboard");
        }
    }
}
